<?php
/**
 * @file
 * blogs.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function blogs_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'blogs';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'blogs';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Blogs';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'infinite_scroll';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['relationships']['uid']['required'] = TRUE;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = '';
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = '';
  $handler->display->display_options['fields']['created']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['created']['date_format'] = 'short';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Content: Short Summary */
  $handler->display->display_options['fields']['field_blog_teaser_text']['id'] = 'field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['table'] = 'field_data_field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['field'] = 'field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['label'] = '';
  $handler->display->display_options['fields']['field_blog_teaser_text']['alter']['max_length'] = '200';
  $handler->display->display_options['fields']['field_blog_teaser_text']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_blog_teaser_text']['element_label_colon'] = FALSE;
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Authored by';
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_user'] = FALSE;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );

  /* Display: Blogs Landing */
  $handler = $view->new_display('block', 'Blogs Landing', 'blogs_landing');
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Has taxonomy term ID */
  $handler->display->display_options['arguments']['tid']['id'] = 'tid';
  $handler->display->display_options['arguments']['tid']['table'] = 'taxonomy_index';
  $handler->display->display_options['arguments']['tid']['field'] = 'tid';
  $handler->display->display_options['arguments']['tid']['default_action'] = 'default';
  $handler->display->display_options['arguments']['tid']['default_argument_type'] = 'taxonomy_tid';
  $handler->display->display_options['arguments']['tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['tid']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['tid']['specify_validation'] = TRUE;
  $handler->display->display_options['arguments']['tid']['validate']['fail'] = 'ignore';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'blog' => 'blog',
  );

  /* Display: Recent Blogs */
  $handler = $view->new_display('block', 'Recent Blogs', 'recent_blogs');
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['pager']['options']['items_per_page'] = '3';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Teaser Title */
  $handler->display->display_options['fields']['field_blog_teaser_title']['id'] = 'field_blog_teaser_title';
  $handler->display->display_options['fields']['field_blog_teaser_title']['table'] = 'field_data_field_blog_teaser_title';
  $handler->display->display_options['fields']['field_blog_teaser_title']['field'] = 'field_blog_teaser_title';
  $handler->display->display_options['fields']['field_blog_teaser_title']['label'] = '';
  $handler->display->display_options['fields']['field_blog_teaser_title']['element_label_colon'] = FALSE;
  /* Field: Content: Short Summary */
  $handler->display->display_options['fields']['field_blog_teaser_text']['id'] = 'field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['table'] = 'field_data_field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['field'] = 'field_blog_teaser_text';
  $handler->display->display_options['fields']['field_blog_teaser_text']['label'] = '';
  $handler->display->display_options['fields']['field_blog_teaser_text']['alter']['max_length'] = '70';
  $handler->display->display_options['fields']['field_blog_teaser_text']['alter']['trim'] = TRUE;
  $handler->display->display_options['fields']['field_blog_teaser_text']['element_label_colon'] = FALSE;
  /* Field: Content: Teaser class */
  $handler->display->display_options['fields']['field_blog_teaser_class']['id'] = 'field_blog_teaser_class';
  $handler->display->display_options['fields']['field_blog_teaser_class']['table'] = 'field_data_field_blog_teaser_class';
  $handler->display->display_options['fields']['field_blog_teaser_class']['field'] = 'field_blog_teaser_class';
  $handler->display->display_options['fields']['field_blog_teaser_class']['label'] = '';
  $handler->display->display_options['fields']['field_blog_teaser_class']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_blog_teaser_class']['element_label_colon'] = FALSE;
  /* Field: Content: Path */
  $handler->display->display_options['fields']['path']['id'] = 'path';
  $handler->display->display_options['fields']['path']['table'] = 'node';
  $handler->display->display_options['fields']['path']['field'] = 'path';
  $handler->display->display_options['fields']['path']['label'] = '';
  $handler->display->display_options['fields']['path']['exclude'] = TRUE;
  $handler->display->display_options['fields']['path']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['path']['absolute'] = TRUE;
  /* Field: Content: Button text */
  $handler->display->display_options['fields']['field_blog_button_text']['id'] = 'field_blog_button_text';
  $handler->display->display_options['fields']['field_blog_button_text']['table'] = 'field_data_field_blog_button_text';
  $handler->display->display_options['fields']['field_blog_button_text']['field'] = 'field_blog_button_text';
  $handler->display->display_options['fields']['field_blog_button_text']['label'] = '';
  $handler->display->display_options['fields']['field_blog_button_text']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_blog_button_text']['alter']['path'] = '[path]';
  $handler->display->display_options['fields']['field_blog_button_text']['alter']['link_class'] = '[field_blog_teaser_class]';
  $handler->display->display_options['fields']['field_blog_button_text']['element_label_colon'] = FALSE;
  /* Field: Content: Teaser Image */
  $handler->display->display_options['fields']['field_blog_teaser_image']['id'] = 'field_blog_teaser_image';
  $handler->display->display_options['fields']['field_blog_teaser_image']['table'] = 'field_data_field_blog_teaser_image';
  $handler->display->display_options['fields']['field_blog_teaser_image']['field'] = 'field_blog_teaser_image';
  $handler->display->display_options['fields']['field_blog_teaser_image']['label'] = '';
  $handler->display->display_options['fields']['field_blog_teaser_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_blog_teaser_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_blog_teaser_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  $export['blogs'] = $view;

  $view = new view();
  $view->name = 'tags';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Tags';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Tags';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Relationship: Taxonomy term: Content using Tags */
  $handler->display->display_options['relationships']['reverse_field_tags_node']['id'] = 'reverse_field_tags_node';
  $handler->display->display_options['relationships']['reverse_field_tags_node']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['relationships']['reverse_field_tags_node']['field'] = 'reverse_field_tags_node';
  $handler->display->display_options['relationships']['reverse_field_tags_node']['required'] = TRUE;
  /* Field: COUNT(Taxonomy term: Term ID) */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['group_type'] = 'count';
  $handler->display->display_options['fields']['tid']['label'] = '';
  $handler->display->display_options['fields']['tid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['tid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['tid']['separator'] = '';
  /* Field: Taxonomy term: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['label'] = '';
  $handler->display->display_options['fields']['name']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['name']['alter']['text'] = '[name] ([tid])';
  $handler->display->display_options['fields']['name']['alter']['word_boundary'] = FALSE;
  $handler->display->display_options['fields']['name']['alter']['ellipsis'] = FALSE;
  $handler->display->display_options['fields']['name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['name']['link_to_taxonomy'] = TRUE;
  /* Filter criterion: Taxonomy vocabulary: Machine name */
  $handler->display->display_options['filters']['machine_name']['id'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['table'] = 'taxonomy_vocabulary';
  $handler->display->display_options['filters']['machine_name']['field'] = 'machine_name';
  $handler->display->display_options['filters']['machine_name']['value'] = array(
    'tags' => 'tags',
  );

  /* Display: Tags */
  $handler = $view->new_display('block', 'Tags', 'tags');
  $export['tags'] = $view;

  return $export;
}
