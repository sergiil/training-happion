<?php
/**
 * @file
 * front_page.panels_default.inc
 */

/**
 * Implements hook_default_panels_mini().
 */
function front_page_default_panels_mini() {
  $export = array();

  $mini = new stdClass();
  $mini->disabled = FALSE; /* Edit this to true to make a default mini disabled initially */
  $mini->api_version = 1;
  $mini->name = 'footer_panel';
  $mini->category = '';
  $mini->admin_title = 'Footer panel';
  $mini->admin_description = 'Includes footer menus and copyright info.';
  $mini->requiredcontexts = array();
  $mini->contexts = array();
  $mini->relationships = array();
  $display = new panels_display();
  $display->layout = 'twocol';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'left' => NULL,
      'right' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = '62df09f0-8f38-4c52-8081-6e42392e9727';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-fd097fb2-30d8-4ebd-9afd-b572317316b1';
    $pane->panel = 'left';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Join us',
      'title' => '',
      'body' => '<p><a href="http://twitter.com">Join us</a></p>
',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'footer-twitter',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = 'fd097fb2-30d8-4ebd-9afd-b572317316b1';
    $display->content['new-fd097fb2-30d8-4ebd-9afd-b572317316b1'] = $pane;
    $display->panels['left'][0] = 'new-fd097fb2-30d8-4ebd-9afd-b572317316b1';
    $pane = new stdClass();
    $pane->pid = 'new-43a9d4ba-b144-4941-b71f-1dbe252374d7';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'menu-menu-footer-menu';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<none>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '43a9d4ba-b144-4941-b71f-1dbe252374d7';
    $display->content['new-43a9d4ba-b144-4941-b71f-1dbe252374d7'] = $pane;
    $display->panels['right'][0] = 'new-43a9d4ba-b144-4941-b71f-1dbe252374d7';
    $pane = new stdClass();
    $pane->pid = 'new-24981a9b-894a-48ad-9107-a69fb9b5925b';
    $pane->panel = 'right';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'admin_title' => 'Copyright info',
      'title' => '',
      'body' => '© @date.year Happion. All rights reserved.',
      'format' => 'full_html',
      'substitute' => TRUE,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'pane-custom-copyright',
    );
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '24981a9b-894a-48ad-9107-a69fb9b5925b';
    $display->content['new-24981a9b-894a-48ad-9107-a69fb9b5925b'] = $pane;
    $display->panels['right'][1] = 'new-24981a9b-894a-48ad-9107-a69fb9b5925b';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = 'new-43a9d4ba-b144-4941-b71f-1dbe252374d7';
  $mini->display = $display;
  $export['footer_panel'] = $mini;

  return $export;
}
