<?php
/**
 * @file
 * settings.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function settings_user_default_roles() {
  $roles = array();

  // Exported role: content moderator.
  $roles['content moderator'] = array(
    'name' => 'content moderator',
    'weight' => 3,
  );

  return $roles;
}
